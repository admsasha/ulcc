#include <QApplication>

#include <QDir>
#include <QTextCodec>

#include "mainwindow.h"

int main(int argc, char *argv[]){
    QApplication app(argc, argv);

    QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));
    QDir::setCurrent(QApplication::applicationDirPath());

    MainWindow *window = new MainWindow();
    window->show();

    return app.exec();
}
